package com.moravskiyandriy.launch.deprealizations;

import com.moravskiyandriy.Constants;
import com.moravskiyandriy.launch.Deployer;
import com.moravskiyandriy.launch.BaseServerDeployer;
import com.moravskiyandriy.service.RESTAccountServiceImpl;
import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Optional;
import java.util.Scanner;

public class RESTServerDeployer1 extends BaseServerDeployer implements Deployer {
    private final static int PORT = Optional.
            ofNullable(getProperties().getProperty("PORT")).
            map(Integer::valueOf).orElse(Constants.PORT);
    private final static String HOST = Optional.
            ofNullable(getProperties().getProperty("HOST"))
            .orElse(Constants.HOST);
    private final static String URI = Optional.
            ofNullable(getProperties().getProperty("URI"))
            .orElse(Constants.URI);
    private Scanner scanner = new Scanner(System.in);

    @Override
    public String deploy() {
        URI baseUri = UriBuilder.fromUri(HOST).port(PORT).path(URI).build();
        ResourceConfig config = new ResourceConfig(RESTAccountServiceImpl.class);
        HttpServer server = JdkHttpServerFactory.createHttpServer(baseUri, config);
        String val = "";
        while (!val.equals("q")) {
            System.out.println("Server is ready.\ninput:");
            val = scanner.nextLine();
            if (val.equals("q")) {
                server.stop(0);
                break;
            }
        }
        return "end";
    }
}
