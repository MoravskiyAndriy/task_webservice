package com.moravskiyandriy.profile;

import com.moravskiyandriy.Constants;
import com.moravskiyandriy.model.Account;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Profile implements Serializable {
    private static List<Account> accountList = new ArrayList<>();

    public Profile(){}

    public String addAccount(BigDecimal sum) {
        String message;
        int maxId = accountList.stream().mapToInt(a -> a.getId()).max().orElse(0);
        maxId++;
        Account account = new Account();
        account.setId(maxId);
        if (sum.compareTo(new BigDecimal(0)) > 0 && sum.compareTo(new BigDecimal(1000)) < 0) {
            account.setBalance(sum);
            accountList.add(account);
            message ="Account with id="+account.getId()+" ";
            message += Constants.ACCOUNT_CREATED;
        } else {
            message = Constants.ACCOUNT_NOT_CREATED;
        }
        return message;
    }

    public Account getAccount(int id) {
        Account account = accountList.stream().filter(a -> a.getId() == id).findFirst().orElse(null);
        return account;
    }

    public String deleteAccountById(int id) {
        String message;
        int beforeSize = accountList.size();
        Account account = getAccount(id);
        accountList = accountList.stream().filter(a -> a.getId() != id).collect(Collectors.toList());
        int afterSize = accountList.size();
        if (afterSize == beforeSize) {
            message = Constants.ACCOUNT_NOT_FOUND;
        } else {
            accountList.remove(account);
            message = Constants.ACCOUNT_DELETED;
        }
        return message;
    }

    public List<Account> getAllAccounts() {
        return accountList;
    }
}
