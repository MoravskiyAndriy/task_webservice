package com.moravskiyandriy.launch;

public interface Deployer {
    String deploy();
}
