package com.moravskiyandriy;

import com.moravskiyandriy.launch.GenDeployer;
import com.moravskiyandriy.launch.deployerimpl.RestDeployer;
import com.moravskiyandriy.launch.deployerimpl.SoapDeployer;

import java.util.Scanner;



public class Launcher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose server: 's'-SOAP, 'r'-REST");
        String res = scanner.nextLine();
        GenDeployer genDeployer;
        while (!(res.equals("s") || res.equals("r"))) {
            res = scanner.nextLine();
        }
        if (res.equals("r")) {
            genDeployer = new RestDeployer();
        } else {
            genDeployer = new SoapDeployer();
        }
        genDeployer.deployServer();
        System.exit(0);
    }
}
