package com.moravskiyandriy.service;

import com.google.gson.Gson;
import com.moravskiyandriy.Constants;
import com.moravskiyandriy.model.Account;
import com.moravskiyandriy.profile.Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

@Path("/accounts")
public class RESTAccountServiceImpl implements RESTAccountService {
    private static final Logger logger = LogManager.getLogger(RESTAccountServiceImpl.class);
    private Profile profile = new Profile();

    @GET
    @Produces("text/plain")
    @Path("/replenishAccount/{inputid}/{inputsum}")
    @Override
    public String replenish(@PathParam("inputid") int id, @PathParam("inputsum") String sum) {
        if (new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).compareTo(Constants.MIN_SUM) < 0) {
            return Constants.SUM_TOO_SMALL;
        }
        String message = Constants.NO_ACCOUNT_WITH_SUCH_ID;
        BigDecimal value = new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP);
        Account account = profile.getAccount(id);
        if (Objects.nonNull(account)) {
            if (account.getBalance().add(value).compareTo(Account.getMaxBalance()) <= 0) {
                account.setBalance(account.getBalance().add(value));
                message = Constants.BALANCE_REPLENISHED_OK;
            } else {
                message = Constants.BALANCE_REPLENISHED_BAD;
            }
        }
        logger.info(" replenish for id= "+id+" sum= "+value+"; result: "+message);
        return message;
    }

    @GET
    @Produces("text/plain")
    @Path("/exhaustAccount/{inputid}/{inputsum}")
    @Override
    public String exhaust(@PathParam("inputid") int id, @PathParam("inputsum") String sum) {
        if (new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).compareTo(Constants.MIN_SUM) < 0) {
            return Constants.SUM_TOO_SMALL;
        }
        String message = Constants.NO_ACCOUNT_WITH_SUCH_ID;
        BigDecimal value = new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP);
        Account account = profile.getAccount(id);
        if (Objects.nonNull(account)) {
            if (account.getBalance().subtract(value).compareTo(Account.getCreditLimit()) >= 0) {
                account.setBalance(account.getBalance().subtract(value));
                message = Constants.BALANCE_EXHAUSTED_OK;
            } else {
                message = Constants.BALANCE_EXHAUSTED_BAD;
            }
        }
        logger.info(" exhaust for id= "+id+" sum= "+value+"; result: "+message);
        return message;
    }

    @GET
    @Produces("text/plain")
    @Path("/getBalance/{inputid}")
    @Override
    public String getBalance(@PathParam("inputid") int id) {
        String message = Constants.NO_ACCOUNT_WITH_SUCH_ID;
        Account account = profile.getAccount(id);
        if (Objects.nonNull(account)) {
            message = account.toString();
        }
        logger.info(" getBalance for id= "+id+"; result: "+message);
        return message;
    }

    @GET
    @Produces("text/plain")
    @Path("/deleteAccount/{inputid}")
    @Override
    public String deleteAccountById(@PathParam("inputid") int id) {
        String message = profile.deleteAccountById(id);
        logger.info(" deleteAccountById for id= "+id+"; result: "+message);
        return message;
    }

    @GET
    @Produces("text/plain")
    @Path("/addAccount/{inputsum}")
    @Override
    public String addAccount(@PathParam("inputsum") String sum) {
        String message;
        if (new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).compareTo(Constants.MIN_SUM) < 0) {
            message = Constants.SUM_TOO_SMALL;
        } else{
            message = profile.addAccount(new BigDecimal(sum));
        }
        logger.info(" addAccount for sum= "+new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP)+
                "; result: "+message);
        return message;
    }

    @GET
    @Produces("text/plain")
    @Path("/getAllAccounts")
    @Override
    public String getAllAccounts() {
        StringBuilder message= new StringBuilder();
        List<Account> list = profile.getAllAccounts();
        for(Account a: list){
            message.append(a.toString()).append(" ");
        }
        logger.info(" getAllAccounts; result: "+message.toString());
        return new Gson().toJson(list);
    }
}
