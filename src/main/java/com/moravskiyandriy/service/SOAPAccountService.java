package com.moravskiyandriy.service;

import com.moravskiyandriy.model.Account;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface SOAPAccountService {

    String replenish(int id, String sum);

    String exhaust(int id, String sum);

    String getBalance(int id);

    String deleteAccountById(int id);

    String addAccount(String sum);

    List<Account> getAllAccounts();
}
