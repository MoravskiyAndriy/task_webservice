package com.moravskiyandriy.launch.deprealizations;

import com.moravskiyandriy.Constants;
import com.moravskiyandriy.launch.BaseServerDeployer;
import com.moravskiyandriy.launch.Deployer;
import com.moravskiyandriy.service.SOAPAccountServiceImpl;

import javax.ws.rs.core.UriBuilder;
import javax.xml.ws.Endpoint;
import java.util.Optional;
import java.util.Scanner;

public class SOAPServerDeployer1  extends BaseServerDeployer implements Deployer {
    private final static int PORT = Optional.
            ofNullable(getProperties().getProperty("PORT")).
            map(Integer::valueOf).orElse(Constants.PORT);
    private final static String HOST = Optional.
            ofNullable(getProperties().getProperty("HOST"))
            .orElse(Constants.HOST);
    private final static String URI = Optional.
            ofNullable(getProperties().getProperty("URI"))
            .orElse(Constants.URI);
    private Scanner scanner = new Scanner(System.in);

    @Override
    public String deploy() {
        Endpoint ep = Endpoint.create(new SOAPAccountServiceImpl());
        java.net.URI baseUri = UriBuilder.fromUri(HOST).port(PORT).path(URI).build();
        ep.publish(baseUri.toString()+"accounts?wsdl");
        String val = "";
        while (!val.equals("q")) {
            System.out.println("Server is ready.\ninput:");
            val = scanner.nextLine();
            if (val.equals("q")) {
                ep.stop();
                break;
            }
        }
        return "end";
    }
}
