package com.moravskiyandriy.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Account implements Serializable {
    private static BigDecimal CREDIT_LIMIT = new BigDecimal(-50);
    private static BigDecimal MAX_BALANCE = new BigDecimal(1000);

    private int id;
    private BigDecimal balance;

    public Account() {
    }

    public Account(int id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public static BigDecimal getCreditLimit() {
        return CREDIT_LIMIT;
    }

    public static BigDecimal getMaxBalance() {
        return MAX_BALANCE;
    }

    @Override
    public String toString() {
        return "Account:  " +
                "id=" + id +
                ", balance=" + balance +
                " UAH";
    }
}
