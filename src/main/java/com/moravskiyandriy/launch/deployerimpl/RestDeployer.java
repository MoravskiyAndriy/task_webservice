package com.moravskiyandriy.launch.deployerimpl;

import com.moravskiyandriy.launch.Deployer;
import com.moravskiyandriy.launch.GenDeployer;
import com.moravskiyandriy.launch.deprealizations.RESTServerDeployer1;

public class RestDeployer extends GenDeployer {
    @Override
    protected Deployer createDeployer() {
        return new RESTServerDeployer1();
    }
}
