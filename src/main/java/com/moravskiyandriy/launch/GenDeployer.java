package com.moravskiyandriy.launch;

public abstract class GenDeployer {
    protected abstract Deployer createDeployer();
    public void deployServer(){
        Deployer deployer=createDeployer();
        System.out.println(deployer.deploy());
    }
}
