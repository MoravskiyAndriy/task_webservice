package com.moravskiyandriy.launch.deployerimpl;

import com.moravskiyandriy.launch.Deployer;
import com.moravskiyandriy.launch.GenDeployer;
import com.moravskiyandriy.launch.deprealizations.SOAPServerDeployer1;

public class SoapDeployer extends GenDeployer {
    @Override
    protected Deployer createDeployer() {
        return new SOAPServerDeployer1();
    }
}
