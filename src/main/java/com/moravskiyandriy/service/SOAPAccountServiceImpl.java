package com.moravskiyandriy.service;

import com.moravskiyandriy.Constants;
import com.moravskiyandriy.model.Account;
import com.moravskiyandriy.profile.Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.WebService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

@WebService(endpointInterface = "com.moravskiyandriy.service.SOAPAccountService")
public class SOAPAccountServiceImpl implements SOAPAccountService {
    private static final Logger logger = LogManager.getLogger(SOAPAccountServiceImpl.class);
    private Profile profile = new Profile();

    @Override
    public String replenish(int id, String sum) {
        if (new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).compareTo(Constants.MIN_SUM) < 0) {
            return Constants.SUM_TOO_SMALL;
        }
        String message = Constants.NO_ACCOUNT_WITH_SUCH_ID;
        BigDecimal value = new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP);
        Account account = profile.getAccount(id);
        if (Objects.nonNull(account)) {
            if (account.getBalance().add(value).compareTo(Account.getMaxBalance()) <= 0) {
                account.setBalance(account.getBalance().add(value));
                message = Constants.BALANCE_REPLENISHED_OK;
            } else {
                message = Constants.BALANCE_REPLENISHED_BAD;
            }
        }
        logger.info(" replenish for id= " + id + " sum= " + value + "; result: " + message);
        return message;
    }

    @Override
    public String exhaust(int id, String sum) {
        if (new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).compareTo(Constants.MIN_SUM) < 0) {
            return Constants.SUM_TOO_SMALL;
        }
        String message = Constants.NO_ACCOUNT_WITH_SUCH_ID;
        BigDecimal value = new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP);
        Account account = profile.getAccount(id);
        if (Objects.nonNull(account)) {
            if (account.getBalance().subtract(value).compareTo(Account.getCreditLimit()) >= 0) {
                account.setBalance(account.getBalance().subtract(value));
                message = Constants.BALANCE_EXHAUSTED_OK;
            } else {
                message = Constants.BALANCE_EXHAUSTED_BAD;
            }
        }
        logger.info(" exhaust for id= " + id + " sum= " + value + "; result: " + message);
        return message;
    }

    @Override
    public String getBalance(int id) {
        String message = Constants.NO_ACCOUNT_WITH_SUCH_ID;
        Account account = profile.getAccount(id);
        if (Objects.nonNull(account)) {
            message = account.toString();
        }
        logger.info(" getBalance for id= " + id + "; result: " + message);
        return message;
    }

    @Override
    public String deleteAccountById(int id) {
        String message = profile.deleteAccountById(id);
        logger.info(" deleteAccountById for id= " + id + "; result: " + message);
        return message;
    }

    @Override
    public String addAccount(String sum) {
        String message;
        if (new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).compareTo(Constants.MIN_SUM) < 0) {
            message = Constants.SUM_TOO_SMALL;
        } else {
            message = profile.addAccount(new BigDecimal(sum));
        }
        logger.info(" addAccount for sum= " + new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP) +
                "; result: " + message);
        return message;
    }

    @Override
    public List<Account> getAllAccounts() {
        StringBuilder message= new StringBuilder();
        List<Account> list = profile.getAllAccounts();
        for(Account a: list){
            message.append(a.toString()).append(" ");
        }
        logger.info(" getAllAccounts; result: " + message.toString());
        return list;
    }
}
