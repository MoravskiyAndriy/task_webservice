package com.moravskiyandriy.service;

public interface RESTAccountService {

    String replenish(int id, String sum);

    String exhaust(int id, String sum);

    String getBalance(int id);

    String deleteAccountById(int id);

    String addAccount(String sum);

    String getAllAccounts();
}
